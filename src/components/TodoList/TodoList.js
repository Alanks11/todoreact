import React from 'react';
import TodoInput from '../TodoInput/TodoInput.js';
import TodoItem from '../TodoItem/TodoItem.js';
import './TodoList.css';

class TodoList extends React.Component {
    constructor(props){
        super(props);
        this.state = JSON.parse(localStorage.getItem('todoState')) || {tasks: []};
    }

    addTask(val){
        this.state.tasks.push(val);
        this.setState(this.state);
        localStorage.setItem('todoState', JSON.stringify(this.state));
        
    }
    removeItem(id){
        this.state.tasks.splice(id, 1);
        this.setState(this.state);
        localStorage.setItem('todoState', JSON.stringify(this.state));

    }
    render(){
        return <div>
            <h1>Todo list</h1>
            <TodoInput addTask={this.addTask.bind(this)}/>
            <ul>
            {this.state.tasks.map((val, ind) => {
                return <TodoItem removeItem={this.removeItem.bind(this)} dataId={ind} key={ind} task={val} />
            })}
            </ul>
        </div>
        
    }
}

export default TodoList;