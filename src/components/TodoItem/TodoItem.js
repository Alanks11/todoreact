import React from 'react';
import './TodoItem.css';


class TodoItem extends React.Component {
    handler(e){
        let id = e.currentTarget.dataset.id;
        this.props.removeItem(id);
    }
    keyboardHandler(e){
        if(e.code === 'Enter' || e.code === 'NumpadEnter'){
            this.handler(e);
        }
     
    }
    render(){
        return <li onKeyUp={this.keyboardHandler.bind(this)} onClick={this.handler.bind(this)} data-id={this.props.dataId} tabIndex='1'>{this.props.task}</li>
    }
}
export default TodoItem;