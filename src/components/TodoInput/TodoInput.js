import React from 'react';
import './TodoInput.css';


class TodoInput extends React.Component {
    constructor(props){
        super(props);
        this.state = {value: ''};
    }
    add(){
        if(this.state.value.trim() !== ''){
            this.props.addTask(this.state.value.trim());
            this.setState({
                value: '',
            })
        }
    }
    setValue(e){
        this.setState({
            value: e.target.value,
        })
    }
    keyHandler(e){
        if(e.code === 'Enter' || e.code === 'NumpadEnter'){
            this.add();
        }
    }
    render(){
        return <div className='input-item'>
            <input onInput={this.setValue.bind(this)} 
            onKeyUp={this.keyHandler.bind(this)} 
            type='text' 
            value={this.state.value} 
            tabIndex='1' 
            autoFocus='autofocus' />
    
            <div onClick={this.add.bind(this)} className="input-button">Add</div>
        </div>
        
    }
}
export default TodoInput;